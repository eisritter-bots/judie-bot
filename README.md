`this README will be updated regularly to fit new update content!`

# Judie bot Project Repository

|  General Info  | |
| ---|---|
| Working Title | `Judie Bot` |
| Final Title | `Judie Bot` |
| Developer | `Eisritter` |
| Target Platform(s) | `Discord` |
| Start Date | July 2021 |
| Current Version | 2.0 |

### Abstract

In this project, I will work on making a discord bot for the Caribdis Games community. Said bot will contain the following list of features, which may be expanded to the developer's and community's wishes:
- OiaLt gf game
- OiaLt & Eternum nsfw command
- Miscellaneous greeting commands
- Account management commands
- Eternum gf game

## Repository Usage Guides

```
RepositoryRoot/
    ├── README.md           // This should reflect the project accurately,
    │                       //  so always contains information about the bot 
    │                       //  and its functions. 
    ├── CHANGELOG           // Logs all update notes for Judie from 1.3.0 onwards!
    ├── code/               // Project code and attachements are in here!
    └── documentation/      // Full documentation & Update logs
```

## Changelog Judie V2.0

**Uploaded 9. September 2022**

Major update (was about time)

### Changes:

- implemented the new Eternum GF Game

- updated NSFW commands to include lewds from Eternum 0.3 and 0.4.

- Added -oprotections and -ocollections command to the OiaLt module.

- made minor changes in the OiaLt.py file, most notably to be more flexible for future updates.

## Judie's Eternum gf game:

Pull a partner from the Eternum universe once every 23h. Different characters can either be collected (or not), or have an impact on said collections.

*Requires registration to the database in order to track progress. This is a protected database and stores only your publicly available discord ID as information.*

### Harem:

Collect Alex, Annie, Dalia, Luna, Nancy, Nova, and Penny for a total of 7 harem members!

Beware! For Thanatos is on a mission to take any valuable items off them at the cost of their life, taking particular pleasure in slaughtering Alex and Nova, unless you manage to make an escape thanks to the skillful Calypso.

Check your progress with -eharem !

### The homies:

Collect Chang, Chop Chop, Mr. Hernandez, Jerry, Micaela, Noah, Orion, and Raul for a total of 8 homies!

Beware, for a bloodthirsty Troll is never far enough from smashing in your best pal's faces, especially when hanging out with Jerry. That is unless you have Dalia around to protect you from it though.

Check your progress with -homies !

### Side Girls:

Collect the Blue Fox Maiden, Calypso, Eva, Idriel, Maat, the Red Fox Maiden, and Wenlin for a total of 7 side girls. 

Beware, because Axel will take any chance he can get to get laid with these fine ladies, unless you have Orion around to deal with it using a good punch.

Check your progress with -sidegirls

### Creatures:

Collect Carolyn, Igor, Kermit, Maurice, Maurice, Maurice, and Xenomorph for a total of 7 faithful creatures.

Beware, because a powerful golem has been summoned to stomp your faithful companions to the ground if you don't have Pyramid Head by your side to deny it.

Check your progress with -creatures

## Judie's OiaLt gf game:

Pull a partner from the Once in a Lifetime universe once every 23h. Different characters can be collected or impact your collections:

*Requires registration to the database in order to track progress. This is a protected database and stores only your publicly available discord ID as information.*

### Harem:

Collect Judie, Lauren, Messy Hair Lauren, Carla, Iris, Jasmine, Aiko and Rebecca for a total of 8 harem members!

Beware! For Orochi will likely buy off a harem member, preferring any version of Lauren, unless you're protected by the Funtime Clan Leader!

Check your progress with -oharem !

### Stabby Clan:

Collect Father Mitchell, Mike the Yakuza, Mike the Exterminator, Mike the Policeman, Mike the Hitman, and Anastasia for a total of 6 stabby clan members!

Beware! For Astaroth will likely kill off a clan member, preferring Father Mitchell, unless you're protected by the Once in a Lifetime MC!

Check your progress with -stabbyclan !

### The Boys:

Collect MC, Tom, Fit Jack, Hiromi, Asmodeus, and Oliver for a total of 6 members of the boys!

Beware! For Azazel will likely kill off one of the boys, preferring the MC, unless you're protected by Aiko!

Check your progress with -theboys !

### Potential LI's:

Collect Ava, the Shop Girl, the Train Conductor, Fit Jack's Groupie, the Stone Elephant, and Lilith for a total of 6 potential LI's!

Beware! For Monster Lilith will likely mutate a potential LI, preferring Lilith, unless 93 gets to chase her away before hand!

Check your progress with -potentialLis !

### More Info:

The protection and membership in a collection are independent from another! Even if Orochi buys off Aiko, she will still protect the boys!
Delete your data stored by Judie any time with -deleteacc!

## Other commands:

### Account management:

- register to the database with '-register'

- update to new database versions with '-update' if required (note: not every update of the bot implies a database update.)

- delete any mention of your account in the database with '-deleteacc', which will prompt a confirmation message before deleting your data.

### Greeting commands:

- -gm (good morning)

- -gn (good night)

### Nsfw command:

use '-nsfw' (In a channel marked as such!) to get a random lewd render from the Once in a Lifetime or Eternum games!

You can add filters by adding one (!) name after the command issued, e.g. -nsfw Annie!

Currently supported filters are:

- OiaLt/Once in a Lifetime

- Eternum

- Judie; Lauren; Carla; Iris; Jasmine; Aiko; Rebecca

- Annie; Dalia; Nancy; Penny/Penelope; Luna (/!\ no lewd renders of her in the game yet!); Nova (/!\ no lewd renders of her in the game yet!); Alex/Alexandra; Eva

(Filter typing is case insensitive, you could write pENeLOpE and it'll give you a juicy penny image)
