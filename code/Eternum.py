# DISCORD.PY
import discord
from discord import Embed, File
from discord.ext import commands, tasks
from discord.ext.commands import cooldown, BucketType
# OTHER HELPING
import random
import sqlite3
# OWN HELPING
from Utilities import Collections, HelperClass, Results, Effects
from CharacterCard import CharacterCard
from AccountManager import AccountManager


class Eternum(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.characters = []
        self.botSpamChannels = []
        self.HelperClass = HelperClass()
        self.accountManager = AccountManager(self.client)
        self.setup()

    # Development Start 17/08/2022; Version 1.0.

    cooldowntime = 82800

    # HELPER FUNCTIONS - checkUser in AccountManager // createEmbed in Utilities/HelperFunctions

    def setup(self):
        # CHARACTER CARDS
        # 0
        Abbott = CharacterCard(name="Professor Abbott",
                               picNumber=1,
                               quotes=["You look like a well-prepared team."],
                               filename="abbott")
        self.characters.append(Abbott)

        # HAREM / 1
        Alex = CharacterCard(name="Alexandra Bardot",
                             picNumber=9,
                             quotes=["What! You're supposed to say no.", "Go hang out with Idriel or something...",
                                     "My prince... bullshit...", "We're a team now... I guess.",
                                     "Don't tell me I've damaged the Bardot royal raisins!",
                                     "Thank you for sticking around for my TED talk, though.",
                                     "Oh, I know what you want, a... you wouldn't be able to handle it, pretty boy."],
                             filename="alex",
                             aliases="Alex, Orion's lil' super-soaker, Neptune",
                             collection=Collections.HAREM)
        self.characters.append(Alex)

        # 2
        Alicia = CharacterCard(name="Alicia Flink",
                               picNumber=1,
                               quotes=["*moans*"],
                               filename="aliciaflink")
        self.characters.append(Alicia)

        # 3
        Anne = CharacterCard(name="Annie Flink",
                             picNumber=1,
                             quotes=["I want Mr. Nebula."],
                             filename="annieflink",
                             aliases="Anne")
        self.characters.append(Anne)

        # HAREM / 4
        Annie = CharacterCard(name="Annie Winters",
                              picNumber=10,
                              quotes=["THIS IS THE BEST DAY OF MY LIFE!",
                                      "Not a worry in mah noggin' homie. I just be... chillaxin' all day!",
                                      "Umm... like a d-date... date? With me?",
                                      "Too excited? Me? I'm like... super chill! Chillin' like a villain on penicillin,"
                                      " bro!", "They can't hurt you if you can't see them..."],
                              filename="annie",
                              aliases="Pluto",
                              collection=Collections.HAREM)
        self.characters.append(Annie)

        # 5
        Apple = CharacterCard(name="Apple Salesman",
                              picNumber=1,
                              quotes=["30% of the net profits are donated towards the preservation of\nthe ocean's "
                                      "species and their habitats."],
                              filename="applemerchant",
                              aliases="One eternal per apple guy")
        self.characters.append(Apple)

        # 6
        Astor = CharacterCard(name="Ms. Astor",
                              picNumber=1,
                              quotes=["Cutie? Any rumours you've heard about lately?"],
                              filename="astor")
        self.characters.append(Astor)

        # 7
        Avery = CharacterCard(name="Avery",
                              picNumber=1,
                              quotes=["Let me go, goddammit! I don't wanna play anymore",
                                      "I'm not into whatever this is!",
                                      "I'm keeping my eyes closed, hun. I only have eyes for my wife anyways..."],
                              filename="avery")
        self.characters.append(Avery)

        # Quotes! / SIDE GIRL HARASSER / 8
        Axel = CharacterCard(name="Axel Bardot",
                             picNumber=2,
                             quotes=["You messed with the wrong guy.",
                                     "I know you're new around here but you have no idea how lucky you are right now.",
                                     "All the girls in school would kill to spend an afternoon with me.",
                                     "You're exactly my type. Quiet and compliant",
                                     "I'm a Bardot. My family could buy out the police if they wanted.",
                                     "I always get what I want in the end, baby.",
                                     "You just signed your death warrant, motherfucker!"],
                             filename="axel",
                             effects=Effects.SIDE_GIRL_KIDNAPPER, )
        self.characters.append(Axel)

        # 9
        Benja = CharacterCard(name="Benjamin Dawson",
                              picNumber=2,
                              quotes=[
                                  "You should show some respect when you're talking to Mr. Bardot, you fucking worm.",
                                  "Want me to smash your face in, cocksucker?", "Man, I feel like a celebrity!"],
                              filename="benjamin",
                              aliases="Benja")
        self.characters.append(Benja)

        # SIDE GIRL / 10
        BlueFoxMaiden = CharacterCard(name="Blue Fox Maiden",
                                      picNumber=1,
                                      quotes=["We hope you're enjoying your stay.",
                                              "We are the gift. Our task is to satisfy all of your desires."],
                                      filename="bluefoxmaiden",
                                      collection=Collections.SIDE_DISHES)
        self.characters.append(BlueFoxMaiden)

        # Needs Quotes! / 11
        Brock = CharacterCard(name="Brock Domen",
                              picNumber=2,
                              quotes=["I call this... the Crested Crane pose.",
                                      "Maybe you can convince them to give you a consolation prize, as the biggest loser?",
                                      "I'll be honest, I never doubted that I'd win this competition.",
                                      "Can you believe she didn't even know the rules? What a bimbo!",
                                      "I have the reflexes of a feral cat",
                                      "Come on, try to hit me, big man. Right here."],
                              filename="brock")
        self.characters.append(Brock)

        # 12
        Bundledore = CharacterCard(name="Balbus Bundledore",
                                   picNumber=3,
                                   quotes=["WELCOME, NEW PUPILS...",
                                           "THAT FUCKING BITCH STOLE MY IDEA 40 YEARS AGO AND MADE IT HER OWN!"],
                                   filename="bundledore",
                                   aliases="Professor Bundledore, Headmaster of\nWarthogs School of Witchcraft and "
                                           "Wizardry.")
        self.characters.append(Bundledore)

        # SIDE GIRL / HAREM SAVER / 13
        Calypso = CharacterCard(name="Calypso",
                                picNumber=9,
                                quotes=["The more forbidden it is, the more thrilling it becomes... would you not "
                                        "agree?",
                                        "Holy Mother of Turska!", "I can do many things.", "Glem ni sa' eradin.",
                                        "Holy Mother of Aen Seidhe, can you not see that the Princess is talking here, "
                                        "you filthy peasant?!!",
                                        "I usually do not like your peasant garb, but this time I was not entirely "
                                        "displeased by them.",
                                        "You look... \"hot as fuck\" as well."],
                                filename="calypso",
                                effects=Effects.HAREM_SAVIOUR,
                                aliases="Daughter of Raewyn, Princess of the Sy-tel-quessir, Princess of the Hyril'ar,\n"
                                        "Carrie, Realm, Gaia, Caly",
                                collection=Collections.SIDE_DISHES)
        self.characters.append(Calypso)

        # 14
        Carolyn = CharacterCard(name="Carolyn",
                                picNumber=1,
                                quotes=["*meows*"],
                                filename="carolyn",
                                collection=Collections.CREATURES)
        self.characters.append(Carolyn)
        # 15
        Cassian = CharacterCard(name="Cassian",
                                picNumber=1,
                                quotes=["It's not every day you get to meet the 'Best tits on Instagram'."],
                                filename="cassian",
                                aliases="Mr. Handsy Man")
        self.characters.append(Cassian)

        # Quotes! / HOMIE / 16
        Chang = CharacterCard(name="Chang Zhong",
                              picNumber=5,
                              quotes=["Well I wouldn't say \"obsessed\", but...", "Say no more. I'M YOUR MAN!",
                                      "Between you and me, mate, I think ya boy is about to GET IT!", "Chang'd",
                                      "Bro, if you believe you're a player, you'll become a player.",
                                      "If all else fails, I'll always be someone you can depend on, my friend.",
                                      "Nothing can cure the soul but the senses,\njust as nothing can cure the senses but the soul",
                                      "Apparently, the dad is on a business trip. You know what that means...",
                                      "Nobody will be safe from Chang the Casanova!",
                                      "Ok, ok, not so loud. Keep it cool... I've got a reputation to uphold.",
                                      "My future family of 13 is in your hands!"],
                              filename="chang",
                              aliases="Chang the Ladies' man, Chang the Socialite, Chang the Heist Meister",
                              collection=Collections.THE_HOMIES)
        self.characters.append(Chang)

        # 17
        Charlotte = CharacterCard(name="Charlotte",
                                  picNumber=2,
                                  quotes=["I don't have saggy tits!!! Stop spreading that rumor!",
                                          "Um... I have a boyfriend."],
                                  filename="charlotte",
                                  aliases="Miss Saggy Tits")
        self.characters.append(Charlotte)

        # Quotes! / HOMIE / 18
        ChopChop = CharacterCard(name="Chop Chop",
                                 picNumber=3,
                                 quotes=["Junk?, Oh, why you try to hurt my achey-breaky heart?",
                                         "Chop-Chop only have best materials! Best products!",
                                         "Where I from, popular saying goes, \"If you can't reach your dreams, use more"
                                         " lube\".", "What *DON'T* I  sell! Me have everything!",
                                         "THANK YOU FRIEND! COME BACK TO CHOP CHOP'S ANOTHER DAY!"],
                                 filename="chopchop",
                                 collection=Collections.THE_HOMIES)
        self.characters.append(ChopChop)

        # Quotes! / HAREM / HOMIE SAVER / 19
        Dalia = CharacterCard(name="Dalia Carter",
                              picNumber=5,
                              quotes=["Come on! Less fright, more bite!!",
                                      "I'm sorry, I was here making small talk as if you've been here for weeks.",
                                      "This machine needs fuel!", "Ass to grass! Ass to grass!",
                                      "Hello there, wanderer.",
                                      "PANCAKES!! YAY!!", "I'm a miserable mess...",
                                      "You're goddamn right! I'm strong! Nothing's gonna stop me!",
                                      "Never let it be said that Dalia doesn't honour her bets."],
                              filename="dalia",
                              effects=Effects.HOMIE_SAVIOUR,
                              aliases="Dalia the Explorer, Mars",
                              collection=Collections.HAREM)
        self.characters.append(Dalia)

        # 20
        DuPont = CharacterCard(name="Dr. Du Pont",
                               picNumber=1,
                               quotes=[
                                   "These programmers are terrible. I bet they hired immigrants.\nOr worse... women.",
                                   "Incompetent..."],
                               filename="dupont")
        self.characters.append(DuPont)

        # 21
        Claudius = CharacterCard(name="Emperor Claudius III",
                                 picNumber=2,
                                 quotes=["Whenever I make an entrance, you must bow before me.",
                                         "A woman? Emperor?! Hahaha! Nonsense. You shall become my concubine.",
                                         "With this woman under my control, I'll finally be able to conquer the\nentire "
                                         "server. I'll become a God!",
                                         "How dare he disobey an order of the Emperor!",
                                         "Why you insolent little... (famous last words)"],
                                 filename="claudius",
                                 aliases="Claudius the Great, Claudius the Magnificent,\nClaudius the Godsend "
                                         "(all self-proclaimed)")
        self.characters.append(Claudius)

        # SIDE GIRL / 22
        Eva = CharacterCard(name="Eva",
                            picNumber=1,
                            quotes=["Are you trying to make me blush? It might be working..."],
                            filename="eva",
                            aliases="Who?",
                            collection=Collections.SIDE_DISHES)
        self.characters.append(Eva)

        # 23
        Founder = CharacterCard(name="The Founder",
                                picNumber=3,
                                quotes=["...", "I must admit that I am impressed.",
                                        "I am sorry, where are my manners. Please, sit down",
                                        "I have been looking forward to meeting you..."],
                                filename="founder")
        self.characters.append(Founder)

        # 24
        Garrington = CharacterCard(name="Cornelius Garrington",
                                   picNumber=2,
                                   quotes=["So cocky. You remind me of myself when I was young.",
                                           "Ah, that must be my men telling me the job is done.\nSPOILER: ||it was not||"],
                                   filename="garringtonsr")
        self.characters.append(Garrington)

        # Quotes needed! / 25
        GarringtonJr = CharacterCard(name="Bennie Garrington",
                                     picNumber=2,
                                     quotes=[
                                         "Maybe we can continue this conversation in my private suite... in my hotel...",
                                         "Thank you. You can fuck off now.",
                                         "Their group tried to kill me not once, but twice!"],
                                     filename="garringtonjr")
        self.characters.append(GarringtonJr)

        # 26
        Gertrude = CharacterCard(name="Gertrude",
                                 picNumber=1,
                                 quotes=["I don't like to brag, but... I'll have you know I'm the president of Kredon's"
                                         " School PTA."],
                                 filename="gertrude",
                                 aliases="President of Kredon's School PTA")
        self.characters.append(Gertrude)

        # 27
        Hasler = CharacterCard(name="Commander Hasler",
                               picNumber=1,
                               quotes=["Turn around reeeeeal slow.", "Thank you for your cooperation."],
                               filename="hasler",
                               aliases="Commander Hasler, leader of the Second Brigade of the Galactic Union")
        self.characters.append(Hasler)

        # SIDE GIRL / 28
        Idriel = CharacterCard(name="Idriel",
                               picNumber=3,
                               quotes=["Are you enjoying your experience in Eternum so far?",
                                       "Maybe you're not asking the right questions..."],
                               filename="idriel",
                               aliases="The Eternum Lady, Eternum's AI",
                               collection=Collections.SIDE_DISHES)
        self.characters.append(Idriel)

        # 29
        Igor = CharacterCard(name="Igor",
                             picNumber=1,
                             quotes=["Mmmm, Igor is hungry for the only thing on the menu -  one nice, biiiiig cock!",
                                     "What's up with this trend among women lately in not having penises?"],
                             filename="igor",
                             collection=Collections.CREATURES)
        self.characters.append(Igor)

        # 30
        Jasticus = CharacterCard(name="Jasticus the Decapitator",
                                 picNumber=1,
                                 quotes=["They won't be able to scrub out your blood from the\nRotunda floors after I'm"
                                         " done with you!", "COME HERE, BOY!",
                                         "Well, thanks again for not killing me, buddy."],
                                 filename="jasticus")
        self.characters.append(Jasticus)

        # HOMIE / 31
        Jerry = CharacterCard(name="Jerry",
                              picNumber=4,
                              quotes=["This face ring a bell?", "I AM THY SAVIOUR",
                                      "I AM THE BEST PLAYERETH IN THIS COUNTRY...ETH",
                                      "I recognize that prepubescent voice."],
                              filename="jerry",
                              aliases="Sir Jermaine of Noriander",
                              collection=Collections.THE_HOMIES)
        self.characters.append(Jerry)

        # 32
        Kai = CharacterCard(name="Kai",
                            picNumber=1,
                            quotes=["Welcome to your *reawakening*."],
                            filename="kai",
                            aliases="Kai from Kainan Engineering")
        self.characters.append(Kai)

        # 33
        Keating = CharacterCard(name="Professor Keating",
                                picNumber=1,
                                quotes=[
                                    "You can do the p-project another day then. N-no problem. F-family comes first."],
                                filename="keating")
        self.characters.append(Keating)

        # 34
        Kermit = CharacterCard(name="Kermit",
                               picNumber=1,
                               quotes=["\*Croaks\*"],
                               filename="kermit",
                               aliases="lil' Kermie",
                               collection=Collections.CREATURES)
        self.characters.append(Kermit)

        # 35
        Kimberly = CharacterCard(name="Kimberly",
                                 picNumber=1,
                                 quotes=["I knew that one! You need to consult with us before answering!"],
                                 filename="kimberly")
        self.characters.append(Kimberly)

        # 36
        Kitty = CharacterCard(name="Kitty",
                              picNumber=1,
                              quotes=[
                                  "Normally an item like this would have a high asking price, but since\nI owe Annie a favour... Consider yourself lucky."],
                              filename="kitty",
                              aliases="Kitytu")
        self.characters.append(Kitty)

        # 37
        Linus = CharacterCard(name="Alastor Linus",
                              picNumber=1,
                              quotes=["For this project, we reached out to Eternum's players to figure out the number "
                                      "one question on everyone's mind... DOES IDRIEL HAVE A PUSSY?!"],
                              filename="linus")
        self.characters.append(Linus)

        # Quotes! / 38
        Lucinda = CharacterCard(name="Lucinda Garcia",
                                picNumber=2,
                                quotes=["Nice to finally meet you!", "Ohh, aren't you a polite and handsome young boy?",
                                        "\*Whispering\* he's so good-looking, isn't he?"],
                                filename="lucinda")
        self.characters.append(Lucinda)

        # Quotes! / HAREM / 39
        Luna = CharacterCard(name="Luna Hernandez",
                             picNumber=6,
                             quotes=["Thank you for everything you said. It was... sweet.",
                                     "I'm sorry. I didn't mean to scare you.",
                                     "I... like the feel of silk and velvet, that's all.",
                                     "At 1 month and 14 days post-mortem... only the bones remain.",
                                     "At least he doesn't call me a weirdo like everyone else.",
                                     "Don't look into the room on the right. My father's corpse is still there."],
                             filename="luna",
                             aliases="Ganymede",
                             collection=Collections.HAREM)
        self.characters.append(Luna)

        # SIDE GIRL / 40
        Maat = CharacterCard(name="Maat",
                             picNumber=2,
                             quotes=["Welcome to my Oasis.", "I feel like playing right now... Don't you?",
                                     "You can ask around. Maat always keeps her promises."],
                             filename="maat",
                             collection=Collections.SIDE_DISHES)
        self.characters.append(Maat)

        # 41
        Mandrake = CharacterCard(name="Dona Mandrake",
                                 picNumber=1,
                                 quotes=["Welcome to Potions!", "My classroom, my rules"],
                                 filename="mandrake",
                                 aliases="Professor Mandrake")
        self.characters.append(Mandrake)

        # 42
        Maurice1 = CharacterCard(name="Maurice (cat)",
                                 picNumber=1,
                                 quotes=["*meows*"],
                                 filename="mauricec",
                                 aliases="Maurice",
                                 collection=Collections.CREATURES)
        self.characters.append(Maurice1)

        # 43
        Maurice2 = CharacterCard(name="Maurice (goat)",
                                 picNumber=1,
                                 quotes=["*bleats*",
                                         "Tired of cleaning up after him? He turn into many meals\nand delicious broth!"],
                                 filename="mauriceg",
                                 aliases="Maurice",
                                 collection=Collections.CREATURES)
        self.characters.append(Maurice2)

        # 44
        Maurice3 = CharacterCard(name="Maurice (toucan)",
                                 picNumber=1,
                                 quotes=["Stockholm", "Maurice very intelligent! He know all the answers!"],
                                 filename="mauricet",
                                 aliases="Maurice",
                                 collection=Collections.CREATURES)
        self.characters.append(Maurice3)

        # 45
        Maximo = CharacterCard(name="Maximo",
                               picNumber=1,
                               quotes=["The word of the Emperor is sacred and must be kept.",
                                       "Please do not resist. I've already dealt with enough troublemakers today.",
                                       "Death or Glory."],
                               filename="maximo",
                               aliases="Dingleberry")
        self.characters.append(Maximo)

        # 46
        Mercer = CharacterCard(name="Doctor Mercer",
                               picNumber=1,
                               quotes=["Let's see what people call me in 50 years..."],
                               filename="mercer")
        self.characters.append(Mercer)

        # HOMIE / 47
        Micaela = CharacterCard(name="Micaela Garcia",
                                picNumber=4,
                                quotes=["I know I can have a rather intimidating appearance at first.",
                                        "You've improved a lot over the last year. I'm impressed!"],
                                filename="micaela",
                                collection=Collections.THE_HOMIES)
        self.characters.append(Micaela)

        # 48
        Millie = CharacterCard(name="Millie",
                               picNumber=1,
                               quotes=["You can do it all here at PUMP IT!"],
                               filename="millie")
        self.characters.append(Millie)

        # 49
        Ambrose = CharacterCard(name="Madam Ambrose",
                                picNumber=1,
                                quotes=["Men from around the world come here for... invigoration"],
                                filename="ambrose")
        self.characters.append(Ambrose)

        # 50
        Moon = CharacterCard(name="Moon",
                             picNumber=1,
                             quotes=["I have to go. See you Monday."],
                             filename="moon")
        self.characters.append(Moon)

        # 51
        Mos = CharacterCard(name="Mr. Mos",
                            picNumber=1,
                            quotes=["No self-respecting man should go around without a good suit."],
                            filename="mos")
        self.characters.append(Mos)

        # HOMIE / 52
        Hernandez = CharacterCard(name="Mr. Hernandez",
                                  picNumber=5,
                                  quotes=["Death is not the scariest fate", "minusfiftypointssaywhat?",
                                          "ORION MY MAN! I KNEW YOU WOULDN'T LET US DOWN!",
                                          "YOU'RE GONNA MAKE MEXICO PROUD!"],
                                  filename="hernandez",
                                  collection=Collections.THE_HOMIES)
        self.characters.append(Hernandez)

        # HAREM / 53
        Nancy = CharacterCard(name="Nancy Carter",
                              picNumber=4,
                              quotes=["What have I done to deserve such chivalry?",
                                      "I'm sorry, I just took a shower and I'm all wet.",
                                      "Family always comes first.",
                                      "I want your eyes on me. You know it'd be quite rude to look away from your Empress.",
                                      "Well, are you enjoying this time with your Empress?",
                                      "I refuse to let those ERE bastards take my city! Take OUR city!",
                                      "Well, I have a city to save.",
                                      "The Macedonians are trying to reconquer Megara. OVER MY DEAD BODY."],
                              filename="nancy",
                              aliases="Nan, Empress of the Western Roman Empire, Saturn",
                              collection=Collections.HAREM)
        self.characters.append(Nancy)

        # 54
        Nikolay = CharacterCard(name="Nikolay",
                                picNumber=1,
                                quotes=["Границы России нигде не заканчиваются."],
                                filename="nikolay")
        self.characters.append(Nikolay)

        # HOMIE / 55
        Noah = CharacterCard(name="Noah",
                             picNumber=3,
                             quotes=["We can help you for money",
                                     "I... haven't seen my... Mom... for a very long time, Mr. Keating",
                                     "You'll never fight alone", "To hell and back, moj brat."],
                             filename="noah",
                             collection=Collections.THE_HOMIES)
        self.characters.append(Noah)

        # HAREM / 56
        Nova = CharacterCard(name="Nova Johnson",
                             picNumber=6,
                             quotes=["Sorry for all the commotion, I just had to make sure!",
                                     "I just happened to be looking at my phone and then you called!",
                                     "Sorry, I saw these donuts and I couldn't resist.",
                                     "STOP. HACKING. ME.",
                                     "I'm a hacker, that's my thing. Would you ask an alcoholic to stop drinking?",
                                     "I might look like a walking cheeto, but I promise you, I do not taste like one.",
                                     "Well, one, I can't read minds. I merely uncover personal information that people "
                                     "so foolishly leave unsecure.",
                                     "From now on, I'll be your personal hacker.",
                                     "It was a breeze! Like stealing candy from a baby!"],
                             filename="nova",
                             aliases="Nova, Delilah Warren, Mercury",
                             collection=Collections.HAREM)
        self.characters.append(Nova)

        # HOMIE / SIDE GIRL SAVER / 57
        Orion = CharacterCard(name="Orion Richards",
                              picNumber=10,
                              quotes=["I was hoping [Calypso] was a lightsaber crossbow or something",
                                      "My hair is a wild beast that cannot be tamed!",
                                      "Learn? Me Orion. Me point to pretty thing. Me press button. Me likey.",
                                      "*\"Thit bitch hurs lust duh chins ti bi i midil\"*",
                                      "I'm gonna look like a deformed potato next to you!",
                                      "It's been a pleasure, Mr. Garrington.",
                                      "It's just sushi, not a joint.",
                                      "Did you know that sex is a death sentence for octopuses?"],
                              filename="orion",
                              effects=Effects.SIDE_GIRL_SAVIOUR,
                              aliases="MC, Orion Holmes, Orion the Merciful, Jupiter",
                              collection=Collections.THE_HOMIES)
        self.characters.append(Orion)

        # 58
        Owler = CharacterCard(name="Helga Owler",
                              picNumber=1,
                              quotes=["Oh god! I didn't hear you coming... How embarrassing!"],
                              filename="owler",
                              aliases="Professor Owler")
        self.characters.append(Owler)

        # HAREM / 59
        Penny = CharacterCard(name="Penelope Carter",
                              picNumber=9,
                              quotes=["All that humidity outside better not turn my hair into a frizzy mess!",
                                      "How can stop be the keyword?",
                                      "Did you know that that Gigachad meme guy from a few years ago tried sliding into"
                                      " my DMs?"],
                              filename="penny",
                              aliases="Penny, miss_penny, best tits on instagram, Venus",
                              collection=Collections.HAREM)
        self.characters.append(Penny)

        # 60
        Philippe = CharacterCard(name="Philippe",
                                 picNumber=2,
                                 quotes=["How can you shine so much, sweetie? I'm going to start\nwearing my sunglasses"
                                         " around you!"],
                                 filename="philippe")
        self.characters.append(Philippe)

        # Quotes! / 61
        Praetorian = CharacterCard(name="Praetorian 1",
                                   picNumber=1,
                                   quotes=["Put your hands up and back away from the girl.",
                                           "You've committed crimes against Eternum's\nGeneral Code of Ethics."],
                                   filename="praetorian1")
        self.characters.append(Praetorian)

        # 62
        Praetorian2 = CharacterCard(name="Praetorian 2",
                                    picNumber=1,
                                    quotes=["Resisting will only make things more painful for you.",
                                            "Surrender now. This is your last warning."],
                                    filename="praetorian2")
        self.characters.append(Praetorian2)

        # 63
        Priscilla = CharacterCard(name="Priscilla Bardot",
                                  picNumber=1,
                                  quotes=["I guess you can get away with whatever you want,\nif you have the finest "
                                          "lawyers money can buy."],
                                  filename="priscilla")
        self.characters.append(Priscilla)

        # 64
        Annabelle = CharacterCard(name="Princess Annabelle",
                                  picNumber=1,
                                  quotes=["How did you get into my castle?!", "You worthless wretches!"],
                                  filename="annabelle",
                                  aliases="Annabelle the Skinner")
        self.characters.append(Annabelle)

        # HOMIE / 65
        Raul = CharacterCard(name="Raul",
                             picNumber=3,
                             quotes=["Do you remember what I told you in the trenches in Sarajevo?",
                                     "Let's have one last dance.", "To hell... and back."],
                             filename="raul",
                             collection=Collections.THE_HOMIES)
        self.characters.append(Raul)

        # SIDE GIRL / 66
        RedFoxMaiden = CharacterCard(name="Red Fox Maiden",
                                     picNumber=1,
                                     quotes=["We hope you're enjoying your stay.",
                                             "We are the gift. Our task is to satisfy all of your desires."],
                                     filename="redfoxmaiden",
                                     collection=Collections.SIDE_DISHES)
        self.characters.append(RedFoxMaiden)

        # 67
        Sandra = CharacterCard(name="Sandra Johnson",
                               picNumber=1,
                               quotes=["If it keeps raining this hard, I might need a ride on Noah's ark..."],
                               filename="sandra",
                               aliases="Nova's mom")
        self.characters.append(Sandra)

        # 68
        Snuggles = CharacterCard(name="Snuggles",
                                 picNumber=2,
                                 quotes=["Me chesty... so much hurty...", "Me only wanted to welcomey...",
                                         "Snuggles... is in big owieeeee...",
                                         "Me hurty so much... I no know if me can snuggle anymore...",
                                         "These... These two girls... Promise me... that... that... They're going to "
                                         "rot in a filthy prison cell like the dirty whores they are."],
                                 filename="snuggles",
                                 aliases="Snuggles the happy bear, Snuggles the sadistic bear")
        self.characters.append(Snuggles)

        # HAREM KILLER / 69
        Thanatos = CharacterCard(name="Thanatos",
                                 picNumber=5,
                                 quotes=["I won't ask again.", "Grr", "I must say, you've done more damage here than "
                                                                      "most.",
                                         "**The strong... prevail. The weak... die.**",
                                         "You are powerless. Inferior. Defective. Weak.", "No one can escape from me."],
                                 filename="thanatos",
                                 effects=Effects.HAREM_KILLER,
                                 aliases="The best Eternum player")
        self.characters.append(Thanatos)

        # 70
        TicketGuy = CharacterCard(name="Tickets Seller",
                                  picNumber=1,
                                  quotes=["Do you have a ticket to watch the upcoming Munus?"],
                                  filename="ticketmaster")
        self.characters.append(TicketGuy)

        # HOMIE KILLER / 71
        Troll = CharacterCard(name="Troll",
                              picNumber=1,
                              quotes=["*groans*"],
                              filename="troll",
                              effects=Effects.HOMIE_KILLER)
        self.characters.append(Troll)

        # 72
        Valentino = CharacterCard(name="Nico Valentino",
                                  picNumber=2,
                                  quotes=["Who the fuck are you? Her manager?", "Are you new to this industry or "
                                                                                "something?",
                                          "I'm not even asking for a blowjob this time"],
                                  filename="valentino",
                                  aliases="the best fashion agent in the country")
        self.characters.append(Valentino)

        # 73
        Vasil = CharacterCard(name="Vasil",
                              picNumber=1,
                              quotes=["Bring the egg and I'll open the door, I promise..."],
                              filename="vasil")
        self.characters.append(Vasil)

        # SIDE GIRL / 74
        Wenlin = CharacterCard(name="Wenlin",
                               picNumber=2,
                               quotes=["I was starting to think this server only had stingy, mean people!",
                                       "Business first, pleasure later!", ""],
                               filename="wenlin",
                               collection=Collections.SIDE_DISHES)
        self.characters.append(Wenlin)

        # 75
        William = CharacterCard(name="William Bardot",
                                picNumber=3,
                                quotes=["Hmm, where did I go wrong with you...?",
                                        "LOOK ME IN THE EYE WHEN I'M TALKING TO YOU!",
                                        "Excuse me..? I *know* you aren't talking to me like that.",
                                        "You clearly have no idea who you're speaking to, so let me\nspell it out for your simpleminded brain.",
                                        "You're not even worth the trouble.",
                                        "I'm surrounded by idiots fucking everywhere."],
                                filename="william")
        self.characters.append(William)

        # 76
        Xeno = CharacterCard(name="Xenomorph",
                             picNumber=1,
                             quotes=["\*Squeals\*"],
                             filename="xenomorph",
                             collection=Collections.CREATURES)
        self.characters.append(Xeno)

        # Quotes! / 77
        Con = CharacterCard(name="Con",
                            picNumber=1,
                            quotes=["I knew that fucking letter was a scam.",
                                    "Oh no, an ambush! What am I gonna do... I'm so *scaaaaared*",
                                    "You're in the presence of Eternum's future king!"],
                            filename="con")
        self.characters.append(Con)

        # 78
        Praetorian3 = CharacterCard(name="Praetorian 3",
                                    picNumber=1,
                                    quotes=[
                                        "We're taking all necessary precautions to ensur\nthe security and safety of the players attending tonight."],
                                    filename="praetorian3")
        self.characters.append(Praetorian3)

        # 79
        Pyra = CharacterCard(name="Pyramid Head",
                             picNumber=1,
                             quotes=[""],
                             filename="pyramid_head",
                             effects=Effects.CREATURE_SAVIOUR)
        self.characters.append(Pyra)

        # 80
        Golem = CharacterCard(name="Golem",
                              picNumber=1,
                              quotes=["*groans*"],
                              filename="golem",
                              effects=Effects.CREATURE_STOMPER)
        self.characters.append(Golem)

        # OTHER
        # cari:
        # self.botSpamChannels.append(self.client.get_channel(779873459756335104))
        # test:

    async def buildCharacterEmbed(self, character: CharacterCard, results: Results, ctx):
        embed = ""
        image = ""
        text = ""
        footer = ""
        field_name = ""
        field_value = ""
        color = HelperClass.eternumBlue

        collection = character.collection
        print(f"duplicate: {results.duplicate}\nprotected: {results.protected}\nvictim: {results.victim}")

        if collection == Collections.NONE:
            text = f"{random.choice(character.quotes)}"
            field_name = f"{str(collection)} - {str(character.effects)}"
            field_value = f"*a.k.a. {character.aliases}*"
            footer = f"Better luck next time, {ctx.author.mention}!"
            number = random.randint(1, character.picNumber)
            image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png", filename="gf.png")

            if character.effects in [Effects.HAREM_KILLER, Effects.HOMIE_KILLER, Effects.SIDE_GIRL_KIDNAPPER,
                                     Effects.CREATURE_STOMPER]:
                if character.name == "Thanatos":
                    if results.protected:
                        text = f"Calypso managed to evacuate {results.victim} before Thanatos could kill her! {HelperClass.annieYay}"
                        field_name = f"{str(character.effects)} (denied) {HelperClass.novaGun}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.red
                        footer = f"You won't be this lucky next time, {ctx.author.mention}..."
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_denied.png",
                                             filename="gf.png")
                    else:
                        field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        if results.victim != "Nobody":
                            text = f"Thanatos killed {results.victim}. She is no longer in your harem, " \
                                   f"{ctx.author.mention}! {HelperClass.pepeCry2}"
                        else:
                            text = f"Thanatos didn't find anyone to kill... Lucky you I guess, {ctx.author.mention}."
                        color = HelperClass.black
                        footer = random.choice(character.quotes)
                        number = random.randint(1, character.picNumber)
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png",
                                             filename="gf.png")
                elif character.name == "Troll":
                    if results.protected:
                        text = f"Dalia managed to kill the troll before it could get its hands on {ctx.author.mention}'s" \
                               f" {results.victim}! {HelperClass.annieYay}"
                        field_name = f"{str(character.effects)} (denied) {HelperClass.novaGun}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.red
                        footer = "*groans*"
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_denied.png",
                                             filename="gf.png")
                    else:
                        field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        if results.victim != "Nobody":
                            text = f"The troll killed {results.victim}. They are no longer in your homie group, " \
                                   f"{ctx.author.mention}! {HelperClass.pepeCry2}"
                        else:
                            text = f"The troll didn't find anyone to kill... Lucky you I guess, {ctx.author.mention}"
                        color = HelperClass.black
                        footer = random.choice(character.quotes)
                        number = random.randint(1, character.picNumber)
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png",
                                             filename="gf.png")
                elif character.name == "Axel Bardot":
                    if results.protected:
                        text = f"Orion punched Axel the second he noticed him harassing {results.victim}! {HelperClass.annieYay}"
                        field_name = f"{str(character.effects)} (denied) {HelperClass.novaGun}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.red
                        footer = f"You messed with the wrong guy, cocksucker ({ctx.author.mention})!"
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_denied.png",
                                             filename="gf.png")
                    else:
                        if results.victim != "Nobody":
                            text = f"Oh no! Axel kidnapped {results.victim} from your side girl harem, {ctx.author.mention}! {HelperClass.pepeCry2}"
                            field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                            field_value = f"*a.k.a. {character.aliases}*"
                        else:
                            text = f"Axel didn't find anyone to kidnap... Lucky you I guess, {ctx.author.mention}."
                            field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                            field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.black
                        footer = random.choice(character.quotes)
                        number = random.randint(1, character.picNumber)
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png",
                                             filename="gf.png")

                elif character.name == "Golem":
                    if results.protected:
                        text = f"Pyramid Head managed to kill the troll before it could get its feet on " \
                               f"{ctx.author.mention}'s {results.victim}! {HelperClass.annieYay}"
                        field_name = f"{str(character.effects)} (denied) {HelperClass.novaGun}"
                        field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.red
                        footer = "*groans*"
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_denied.png",
                                             filename="gf.png")
                    else:
                        if results.victim != "Nobody":
                            text = f"The golem stomped {results.victim} to death. They are no longer in your homie group" \
                                   f", {ctx.author.mention}! {HelperClass.pepeCry2}"
                            field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                            field_value = f"*a.k.a. {character.aliases}*"
                        else:
                            text = f"The golem didn't find anyone to trample on... Lucky you I guess, {ctx.author.mention}."
                            field_name = f"{str(character.effects)} {HelperClass.alexAngry}"
                            field_value = f"*a.k.a. {character.aliases}*"
                        color = HelperClass.black
                        footer = random.choice(character.quotes)
                        number = random.randint(1, character.picNumber)
                        image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png",
                                             filename="gf.png")
        else:
            if collection == Collections.HAREM:
                print("harem embed")
                color = HelperClass.pink
                if not results.duplicate:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (new) {HelperClass.daliaParty} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"New harem member {ctx.author.mention}!"
                else:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (duplicate) {HelperClass.annieCry} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"So close! Maybe next time, {ctx.author.mention}..."
                number = random.randint(1, character.picNumber)
                image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png", filename="gf.png")
                print(f"{character.filename}_{number}.png")

            elif collection == Collections.SIDE_DISHES:
                color = HelperClass.purple
                if not results.duplicate:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (new) {HelperClass.daliaParty} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"New side chick {ctx.author.mention}!"
                else:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (duplicate) {HelperClass.annieCry} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"So close! Maybe next time, {ctx.author.mention}..."
                number = random.randint(1, character.picNumber)
                image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png", filename="gf.png")
                print(f"{character.filename}_{number}.png")

            elif collection == Collections.THE_HOMIES:
                color = HelperClass.yellow
                if not results.duplicate:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (new) {HelperClass.daliaParty} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"New homie {ctx.author.mention}!"
                else:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (duplicate) {HelperClass.annieCry} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"So close! Maybe next time, {ctx.author.mention}..."
                number = random.randint(1, character.picNumber)
                image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png", filename="gf.png")
                print(f"{character.filename}_{number}.png")

            elif collection == Collections.CREATURES:
                color = HelperClass.green
                if not results.duplicate:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (new) {HelperClass.daliaParty} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"New creature {ctx.author.mention}!"
                else:
                    text = f"{random.choice(character.quotes)}"
                    field_name = f"{str(collection)} (duplicate) {HelperClass.annieCry} - {str(character.effects)}"
                    field_value = f"*a.k.a. {character.aliases}*"
                    footer = f"So close! Maybe next time, {ctx.author.mention}..."
                number = random.randint(1, character.picNumber)
                image = discord.File(f"./EternumGfGameImages/{character.filename}_{number}.png", filename="gf.png")
                print(f"{character.filename}_{number}.png")

        embed = await self.HelperClass.createEmbed(title=character.name, text=text, color=color, footer=footer)
        embed.add_field(name=field_name, value=field_value)
        embed.set_image(url="attachment://gf.png")
        await ctx.send(file=image, embed=embed)

    async def updateDatabase(self, uid: int, character: CharacterCard, db, cursor):
        duplicateCharacter = False
        protected = False
        victim = None

        cursor.execute("UPDATE eternum SET last_gf = ? WHERE user_id = ?", [character.name, uid])

        if character.collection == Collections.HAREM:
            cursor.execute("SELECT %s FROM eternum_harem WHERE user_id=?" % character.filename, (uid,))
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE eternum_harem SET %s = ? WHERE user_id=?" % character.filename,
                               [character.name, uid])
            else:
                duplicateCharacter = True
            cursor.execute("UPDATE eternum_harem SET last_girl=? WHERE user_id=?", [character.filename, uid])

        elif character.collection == Collections.SIDE_DISHES:
            cursor.execute("SELECT %s FROM side_girls WHERE user_id=?" % character.filename, [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE side_girls SET %s = ? WHERE user_id=?" % character.filename,
                               [character.name, uid])
            else:
                duplicateCharacter = True
            cursor.execute("UPDATE side_girls SET last_affair=? WHERE user_id=?", [character.filename, uid])

        elif character.collection == Collections.THE_HOMIES:
            cursor.execute("SELECT %s FROM homies WHERE user_id=?" % character.filename, [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE homies SET %s = ? WHERE user_id=?" % character.filename, [character.name, uid])
            else:
                duplicateCharacter = True
            cursor.execute("UPDATE homies SET last_homie=? WHERE user_id=?", [character.filename, uid])

        elif character.collection == Collections.CREATURES:
            cursor.execute("SELECT %s FROM creatures WHERE user_id=?" % character.filename, [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == "NONE":
                cursor.execute("UPDATE creatures SET %s = ? WHERE user_id=?" % character.filename,
                               [character.name, uid])
            else:
                duplicateCharacter = True
            cursor.execute("UPDATE creatures SET last_creature=? WHERE user_id=?", [character.filename, uid])

        if character.effects == Effects.HAREM_KILLER:

            cursor.execute("SELECT alex FROM eternum_harem WHERE user_id=?", [uid])
            alex = cursor.fetchone()

            if alex[0] == 'Alexandra Bardot':
                victim = alex[0]
            else:
                cursor.execute("SELECT nova FROM eternum_harem WHERE user_id=?", [uid])
                nova = cursor.fetchone()
                if nova[0] == 'Nova Johnson':
                    victim = nova[0]
                else:
                    cursor.execute("SELECT last_girl FROM eternum_harem WHERE user_id=?", [uid])
                    lastgf = cursor.fetchone()
                    if lastgf[0] in ['annie', 'dalia', 'luna', 'nancy', 'penny']:
                        for i in range(len(self.characters)):
                            if self.characters[i].filename == lastgf[0]:
                                victim = self.characters[i].name
                    else:
                        victim = "Nobody"

            cursor.execute("SELECT calypso FROM eternum WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0 and victim != "Nobody":
                for i in range(len(self.characters)):
                    if self.characters[i].name == victim:
                        column = self.characters[i].filename
                        cursor.execute("UPDATE eternum_harem SET %s = 'NONE' WHERE user_id=?" % column, [uid])

                cursor.execute("UPDATE eternum_harem SET last_girl='NONE' WHERE user_id=?", [uid])

            else:
                if victim != "Nobody":
                    cursor.execute("UPDATE eternum SET calypso=0 WHERE user_id=?", [uid])
                    protected = True

        if character.effects == Effects.SIDE_GIRL_KIDNAPPER:
            cursor.execute("SELECT last_affair FROM side_girls WHERE user_id=?", [uid])
            lastgf = cursor.fetchone()
            if lastgf[0] in ['bluefoxmaiden', 'calypso', 'eva', 'idriel', 'maat', 'redfoxmaiden', 'wenlin']:
                for i in range(len(self.characters)):
                    if self.characters[i].filename == lastgf[0]:
                        victim = self.characters[i].name
            else:
                victim = "Nobody"

            cursor.execute("SELECT orion FROM eternum WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0 and victim != "Nobody":
                for i in range(len(self.characters)):
                    if self.characters[i].name == victim:
                        column = self.characters[i].filename
                        cursor.execute("UPDATE side_girls SET %s='NONE' WHERE user_id=?" % column, [uid])

                cursor.execute("UPDATE side_girls SET last_affair='NONE' WHERE user_id=?", [uid])

            else:
                if victim != "Nobody":
                    cursor.execute("UPDATE eternum SET orion=0 WHERE user_id=?", [uid])
                    protected = True

        if character.effects == Effects.HOMIE_KILLER:
            cursor.execute("SELECT jerry FROM homies WHERE user_id=?", [uid])
            jerry = cursor.fetchone()

            if jerry[0] == 'Jerry':
                victim = jerry[0]
            else:
                cursor.execute("SELECT last_homie FROM homies WHERE user_id=?", [uid])
                lastgf = cursor.fetchone()
                if lastgf[0] in ['chang', 'orion', 'chopchop', 'hernandez', 'micaela', 'noah', 'raul']:
                    for i in range(len(self.characters)):
                        if self.characters[i].filename == lastgf[0]:
                            victim = self.characters[i].name
                else:
                    victim = "Nobody"

            cursor.execute("SELECT dalia FROM eternum WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0 and victim != "Nobody":
                for i in range(len(self.characters)):
                    if self.characters[i].name == victim:
                        column = self.characters[i].filename
                        cursor.execute("UPDATE homies SET %s='NONE' WHERE user_id=?" % column, [uid])

                    cursor.execute("UPDATE homies SET last_homie='NONE' WHERE user_id=?", [uid])

            else:
                if victim != "Nobody":
                    cursor.execute("UPDATE eternum SET dalia=0 WHERE user_id=?", [uid])
                    protected = True

        if character.effects == Effects.CREATURE_STOMPER:
            cursor.execute("SELECT kermit FROM creatures WHERE user_id=?", [uid])
            kermit = cursor.fetchone()

            if kermit[0] == 'Kermit':
                victim = kermit[0]
            else:
                cursor.execute("SELECT last_creature FROM creatures WHERE user_id=?", [uid])
                lastgf = cursor.fetchone()
                if lastgf[0] in ['carolyn', 'igor', 'mauricec', 'mauriceg', 'mauricet', 'xenomorph']:
                    for i in range(len(self.characters)):
                        if self.characters[i].filename == lastgf[0]:
                            victim = self.characters[i].name
                else:
                    victim = "Nobody"

            cursor.execute("SELECT pyramid_head FROM eternum WHERE user_id=?", [uid])
            protection = cursor.fetchone()

            if protection[0] == 0 and victim != "Nobody":
                for i in range(len(self.characters)):
                    if self.characters[i].name == victim:
                        column = self.characters[i].filename
                        cursor.execute("UPDATE creatures SET %s='NONE' WHERE user_id=?" % column, [uid])

                    cursor.execute("UPDATE creatures SET last_creature='NONE' WHERE user_id=?", [uid])

            else:
                if victim != "Nobody":
                    cursor.execute("UPDATE eternum SET pyramid_head=0 WHERE user_id=?", [uid])
                    protected = True

        if character.effects == Effects.HAREM_SAVIOUR:
            cursor.execute("SELECT calypso FROM eternum WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == 0:
                cursor.execute("UPDATE eternum SET calypso=1 WHERE user_id =?", [uid])

        if character.effects == Effects.SIDE_GIRL_SAVIOUR:
            cursor.execute("SELECT orion FROM eternum WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == 0:
                cursor.execute("UPDATE eternum SET orion=1 WHERE user_id =?", [uid])

        if character.effects == Effects.HOMIE_SAVIOUR:
            cursor.execute("SELECT dalia FROM eternum WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == 0:
                cursor.execute("UPDATE eternum SET dalia=1 WHERE user_id =?", [uid])

        if character.effects == Effects.CREATURE_SAVIOUR:
            cursor.execute("SELECT pyramid_head FROM eternum WHERE user_id=?", [uid])
            duplicate = cursor.fetchone()
            if duplicate[0] == 0:
                cursor.execute("UPDATE eternum SET pyramid_head=1 WHERE user_id =?", [uid])

        db.commit()

        return Results(duplicate=duplicateCharacter, protected=protected, victim=victim)

    # COMMANDS

    @commands.command(aliases=['gfe', 'eternum gf', 'gf eternum'])
    @commands.cooldown(1, cooldowntime, commands.BucketType.user)
    async def egf(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)
            else:
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                # choose random character
                gf = random.choice(self.characters)
                # await ctx.send(f"you got {gf.name}")
                print(f"{ctx.author}, {gf.name}")
                # update database accordingly
                print("proceeding to database update")
                results = await self.updateDatabase(uid=uid, character=gf, cursor=cursor, db=db)
                #   create according embed --> self.buildCharacterEmbed
                print("proceeding to embed creation")
                await self.buildCharacterEmbed(character=gf, results=results, ctx=ctx)
                print("success")

            db.commit()
            cursor.close()

    @commands.command(aliases=['hareme', 'eternum harem', 'harem eternum'])
    async def eharem(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        count = 0
        channelId = ctx.channel

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                #   search thru 'eternum_harem' table for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []
                missing = []
                cursor.execute(
                    "SELECT alex, annie, dalia, luna, nancy, nova, penny FROM eternum_harem WHERE user_id=?", [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        count = count + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        count = count - 1

                if "Alexandra Bardot" not in members:
                    missing.append("Alexandra Bardot")
                if "Annie Winters" not in members:
                    missing.append("Annie Winters")
                if "Dalia Carter" not in members:
                    missing.append("Dalia Carter")
                if "Luna Hernandez" not in members:
                    missing.append("Luna Hernandez")
                if "Nancy Carter" not in members:
                    missing.append("Nancy Carter")
                if "Nova Johnson" not in members:
                    missing.append("Nova Johnson")
                if "Penelope Carter" not in members:
                    missing.append("Penelope Carter")

                #   compile entries to list
                haremlist = "\n".join(members)
                missinglist = "\n".join(missing)

                embed_title = f"Eternum Harem of **{user_name[:-5]}**:"

                if haremlist == "":
                    haremlist = "You haven't collected anyone for your harem yet..."

                if missinglist == "":
                    missinglist = f"You have completed the harem! {HelperClass.daliaParty}"
                #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
                embed = discord.Embed(title=embed_title, color=HelperClass.pink)
                embed.add_field(name=f"Claimed ({count}/7):", value=haremlist)
                embed.add_field(name=f"Missing ({7 - count}/7):", value=missinglist)
                await ctx.send(embed=embed)

    # PSEUDOCODE
    @commands.command(aliases=['thehomies', 'the homies', 'da homies', 'ehomies'])
    async def homies(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        count = 0
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                #   search thru 'homies' table for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []
                missing = []
                cursor.execute(
                    "SELECT chang, chopchop, hernandez, jerry, micaela, noah, orion, raul FROM homies WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        count = count + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        count = count - 1

                if "Chang Zhong" not in members:
                    missing.append("Chang Zhong")
                if "Chop Chop" not in members:
                    missing.append("Chop Chop")
                if "Mr. Hernandez" not in members:
                    missing.append("Mr. Hernandez")
                if "Jerry" not in members:
                    missing.append("Jerry")
                if "Micaela Garcia" not in members:
                    missing.append("Micaela Garcia")
                if "Noah" not in members:
                    missing.append("Noah")
                if "Orion Richards" not in members:
                    missing.append("Orion Richards")
                if "Raul" not in members:
                    missing.append("Raul")

                #   compile entries to list
                homielist = "\n".join(members)
                missinglist = "\n".join(missing)

                embed_title = f"Homies of **{user_name[:-5]}**:"

                if homielist == "":
                    homielist = "You haven't collected any of the homies yet..."

                if missinglist == "":
                    missinglist = f"You have assembled all the homies! {HelperClass.daliaParty}"
                #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
                embed = discord.Embed(title=embed_title, color=HelperClass.yellow)
                embed.add_field(name=f"Claimed ({count}/8):", value=homielist)
                embed.add_field(name=f"Missing ({8 - count}/8):", value=missinglist)
                await ctx.send(embed=embed)

    @commands.command(aliases=['sidegirls', 'sidechicks', 'esidegirls', 'epotentiallis'])
    async def sidedishes(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        count = 0
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                #   search thru 'homies' table for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []
                missing = []
                cursor.execute(
                    "SELECT bluefoxmaiden, calypso, eva, idriel, maat, redfoxmaiden, wenlin FROM side_girls WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        count = count + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        count = count - 1

                if "Blue Fox Maiden" not in members:
                    missing.append("Blue Fox Maiden")
                if "Calypso" not in members:
                    missing.append("Calypso")
                if "Eva" not in members:
                    missing.append("Eva")
                if "Idriel" not in members:
                    missing.append("Idriel")
                if "Maat" not in members:
                    missing.append("Maat")
                if "Red Fox Maiden" not in members:
                    missing.append("Red Fox Maiden")
                if "Wenlin" not in members:
                    missing.append("Wenlin")

                #   compile entries to list
                sideslist = "\n".join(members)
                missinglist = "\n".join(missing)

                embed_title = f"Eternum Side Girls of **{user_name[:-5]}**:"

                if sideslist == "":
                    sideslist = "You haven't collected any of the side girls yet..."

                if missinglist == "":
                    missinglist = f"You have assembled all the side girls! {HelperClass.daliaParty}"
                #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
                embed = discord.Embed(title=embed_title, color=HelperClass.purple)
                embed.add_field(name=f"Claimed ({count}/7):", value=sideslist)
                embed.add_field(name=f"Missing ({7 - count}/7):", value=missinglist)
                await ctx.send(embed=embed)

    @commands.command()
    async def creatures(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        count = 0
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                #   search thru 'homies' table for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []
                missing = []
                cursor.execute(
                    "SELECT carolyn, igor, kermit, mauricec, mauriceg, mauricet, xenomorph FROM creatures WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        count = count + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        count = count - 1

                if "Carolyn" not in members:
                    missing.append("Carolyn")
                if "Igor" not in members:
                    missing.append("Igor")
                if "Kermit" not in members:
                    missing.append("Kermit")
                if "Maurice (cat)" not in members:
                    missing.append("Maurice (cat)")
                if "Maurice (goat)" not in members:
                    missing.append("Maurice (goat)")
                if "Maurice (toucan)" not in members:
                    missing.append("Maurice (toucan)")
                if "Xenomorph" not in members:
                    missing.append("Xenomorph")

                #   compile entries to list
                creaturelist = "\n".join(members)
                missinglist = "\n".join(missing)

                embed_title = f"Eternum Creatures of **{user_name[:-5]}**:"

                if creaturelist == "":
                    creaturelist = "You haven't collected any of the creatures yet..."

                if missinglist == "":
                    missinglist = f"You have assembled all of eternum's creatures! {HelperClass.daliaParty}"
                #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
                embed = discord.Embed(title=embed_title, color=HelperClass.green)
                embed.add_field(name=f"Claimed ({count}/7):", value=creaturelist)
                embed.add_field(name=f"Missing ({7 - count}/7):", value=missinglist)
                await ctx.send(embed=embed)

    @commands.command()
    async def eprotectors(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                #   search thru 'eternum_harem' table for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []
                cursor.execute("SELECT orion, calypso, dalia, pyramid_head FROM eternum WHERE user_id = ?", [uid])
                yesno = cursor.fetchone()
                sides = "**Side Girls:**\nOrion: :x:"
                if yesno[0] == 1:
                    sides = "**Side Girls:**\nOrion: ✅"
                members.append(sides)

                harem = "**Harem:**\nCalypso: :x:"
                if yesno[1] == 1:
                    harem = "**Harem:**\nCalypso: ✅"
                members.append(harem)

                homies = "**Homies:**\nDalia: :x:"
                if yesno[2] == 1:
                    homies = "**Homies:**\nDalia: ✅"
                members.append(homies)

                creatures = "**Creatures:**\nPyramid Head: :x:"
                if yesno[3] == 1:
                    creatures = "**Creatures:**\nPyramid Head: ✅"
                members.append(creatures)

                #   compile entries to list
                protectorlist = "\n".join(members)

                embed_title = f"Eternum Protectors of **{user_name[:-5]}**:"
                #   build embed (color pink) with categories 'got x/y' + names & 'missing z/y' + names --> + emotes?
                embed = discord.Embed(title=embed_title, description=protectorlist, color=HelperClass.blue)
                await ctx.send(embed=embed)

    @commands.command(aliases=['eternum collections', 'collectionsE', 'collections eternum'])
    async def eCollections(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)
        haremcount = 0
        homiecount = 0
        sidescount = 0
        creaturecount = 0
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        else:
            # check if user registered & up to date
            checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
            # if no build error embed
            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
            # if yes:
            else:
                embed_title = f"Eternum Collections of **{user_name[:-5]}**:"
                embed = discord.Embed(title=embed_title, color=HelperClass.eternumBlue)

                #   search thru tables for entries
                uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
                members = []

                cursor.execute(
                    "SELECT alex, annie, dalia, luna, nancy, nova, penny FROM eternum_harem WHERE user_id=?", [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        haremcount = haremcount + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        haremcount = haremcount - 1

                #   compile entries to list
                haremlist = "\n".join(members)

                if haremlist == "":
                    haremlist = "You haven't collected anyone for your harem yet..."
                embed.add_field(name=f"Harem: ({haremcount}/7):", value=haremlist)

                members = []

                cursor.execute(
                    "SELECT chang, chopchop, hernandez, jerry, micaela, noah, orion, raul FROM homies WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        homiecount = homiecount + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        homiecount = homiecount - 1

                #   compile entries to list
                homielist = "\n".join(members)

                if homielist == "":
                    homielist = "You haven't collected any of the homies yet..."

                embed.add_field(name=f"Homies: ({homiecount}/8):", value=homielist)

                members = []

                # SIDE GIRLS
                cursor.execute(
                    "SELECT bluefoxmaiden, calypso, eva, idriel, maat, redfoxmaiden, wenlin FROM side_girls WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        sidescount = sidescount + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        sidescount = sidescount - 1

                # compile entries to list
                sideslist = "\n".join(members)

                if sideslist == "":
                    sideslist = "You haven't collected any of the side girls yet..."
                embed.add_field(name=f"Side Girls: ({sidescount}/7):", value=sideslist)

                # Creatures

                members = []

                cursor.execute(
                    "SELECT carolyn, igor, kermit, mauricec, mauriceg, mauricet, xenomorph FROM creatures WHERE user_id=?",
                    [uid])
                yesno = cursor.fetchone()
                for i in yesno:
                    if i != 'NONE':
                        creaturecount = creaturecount + 1
                        members.append(i)
                for j in members:
                    if j == "NONE":
                        members.remove(j)
                        creaturecount = creaturecount - 1

                #   compile entries to list
                creaturelist = "\n".join(members)

                if creaturelist == "":
                    creaturelist = "You haven't collected any of the creatures yet..."
                embed.add_field(name=f"Creatures: ({creaturecount}/7):", value=creaturelist)

                # Protectors

                members = []
                cursor.execute("SELECT orion, calypso, dalia, pyramid_head FROM eternum WHERE user_id = ?", [uid])
                yesno = cursor.fetchone()
                sides = "Side Girls:  :x:"
                if yesno[0] == 1:
                    sides = "Side Girls:  ✅"
                members.append(sides)

                harem = "Harem:  :x:"
                if yesno[1] == 1:
                    harem = "Harem:  ✅"
                members.append(harem)

                homies = "Homies:  :x:"
                if yesno[2] == 1:
                    homies = "Homies:  ✅"
                members.append(homies)

                creatures = "Creatures:  :x:"
                if yesno[3] == 1:
                    creatures = "Creatures:  ✅"
                members.append(creatures)

                #   compile entries to list
                protectorlist = "\n".join(members)

                embed.add_field(name="Protections:", value=protectorlist)

                await ctx.send(embed=embed)

    # ERROR MESSAGES

    @egf.error
    async def errorEgf(self, ctx, error):
        # if cooldown not done send last gf from table 'eternum'
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()

        discordID = str(ctx.author.id)
        uid = await self.accountManager.getUserID(discordID=discordID, cursor=cursor)
        checkUser = await self.accountManager.checkUser(discord_id=discordID, cursor=cursor)
        channelId = ctx.channel
        if self.client.get_channel(779873459756335104) not in self.botSpamChannels:
            self.botSpamChannels.append(self.client.get_channel(779873459756335104))

        if channelId not in self.botSpamChannels:
            embed = discord.Embed(title="Wrong channel!",
                                  description=f"Please take this to {self.botSpamChannels[0].mention}",
                                  color=HelperClass.orange)
            await ctx.send(embed=embed)
        elif isinstance(error, commands.CommandOnCooldown):

            time = error.retry_after
            hours = int(time // 3600)
            minutes = int((time % 3600) // 60)
            seconds = int((time % 3600) % 60)

            description = f"You still have {hours}h {minutes} mins and {seconds}s until your next draw!"

            if checkUser == "register":
                embed = await self.HelperClass.createEmbed(title=f"Error #404 - User {str(ctx.author)} not registered!",
                                                           text="Please register before playing! (-register)",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)

            elif checkUser == "update":
                embed = await self.HelperClass.createEmbed(title=f"Error - User {str(ctx.author)} is not up to date!",
                                                           text="Please update to the newest stand with -update!",
                                                           footer="Contact Eisritter#6969 if you encounter any issues!")
                await ctx.send(embed=embed)
                ctx.command.reset_cooldown(ctx)
            else:
                cursor.execute("SELECT last_gf FROM eternum WHERE user_id=?", [uid])
                lastGf = cursor.fetchone()
                gf = ""
                field_name = ""
                field_value = ""
                field2_name = ""
                field2_value = ""
                number = 0
                if lastGf is None:
                    title = "This is awkward..."
                    field_name = "Your last pull is... No one?"
                    field_value = "How could that happen..."
                    footer = "Might as well contact Eisritter#6969, sumn ain't right"

                else:
                    for i in range(len(self.characters)):
                        if self.characters[i].name == lastGf[0]:
                            gf = self.characters[i]

                    title = "Slow down dude!"
                    field_name = gf.name
                    field_value = f"The last pull you made was {gf.name}"
                    field2_name = f"{str(gf.collection)} - {str(gf.effects)}"
                    field2_value = f"a.k.a. *{gf.aliases}*"
                    footer = "retry later mate..."

                embed = discord.Embed(title=title, description=description, color=HelperClass.eternumBlue)
                embed.set_footer(text=footer)

                embed.add_field(name=field_name, value=field_value, inline=True)
                embed.add_field(name=field2_name, value=field2_value, inline=False)
                if lastGf is not None:
                    number = random.randint(1, gf.picNumber)
                    image = discord.File(f"./EternumGfGameImages/{gf.filename}_{number}.png", filename="gf.png")
                else:
                    image = discord.File("./EternumGfGameImages/None.png", filename="gf.png")
                embed.set_image(url="attachment://gf.png")
                await ctx.send(file=image, embed=embed)

            cursor.close()


def setup(client):
    client.add_cog(Eternum(client))
