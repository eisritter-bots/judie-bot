# New Major Update - JudieBOT V1.3.0
**Uploaded 18. January 2022**

*Update requires updating the database. It's only a minor change, you won't be able to register again, so no risk of data loss normally.*

### Changelog:

- Made the gf game and nsfw commands to separate cogs (plugins if you prefer) to make my reading & maintenance of the code more enjoyable and reduce chances of bugs.

- Adding two new characters to the gf game!
welcoming the *Train Conductor* (now a potential LI, replacing the priestess) and *Fake Hiromi* to the game!
Anyone who had the priestess before the 1.3.0 update will get the Conductor in their collection automatically. Fake Hiromi has no effect whatsoever on the game.

- Adding a brand new kind of content with the nsfw plugin, which will allow you to roll a lot of lewd images from the Once in a Lifetime and Eternum game, with applicable filters! 
The -nsfw command will give you access to one random image of the selected ones. Please note that, as there are no lewd renders of Luna and Nova yet, their filter displays a little troll.
Currently supported filters are (case insensitive): OiaLt, Eternum, Judie, Lauren, Carla, Iris, Jasmine, Aiko, Rebecca, Annie, Dalia, Nancy, Penny/Penelope, Alex/Alexandra, Eva, (Luna, Nova).

- The Judie & Lauren good morning and good night images make their comeback with the -gm and -gn commands!

###  Minor changes include:
- A revisit of the deleteacc command, to ask for confirmation before deleting an account's data
- Some code cleaning
- The move of the client token to a separate file to protect Judie from malicious people among you guys.

**Special thanks to Kainan for helping with the funding to host Judie, as well as the beta testing team who, despite being the biggest spammers I know did help a lot in the process.**